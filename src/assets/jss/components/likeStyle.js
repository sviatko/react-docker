import { makeStyles } from "@material-ui/core/styles";
import { white } from "..";

const likeStyle = {
  root: {
    fill: "red",
    position: "absolute",
    bottom: "-12px",
    border: `2px solid ${white}`,
    left: "10px",
    backgroundColor: white,
    borderRadius: "50px",
    fontSize: "18px",
  },
};

export default makeStyles(likeStyle);

import { makeStyles } from "@material-ui/core/styles";
import { defaultPadding } from "../index";

const useStyles = makeStyles({
  root: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: defaultPadding[0],
    "&:last-child": {
      paddingBottom: "12px",
    },
  },
  details: {
    width: "50%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "baseline",
  },
  title: {
    fontWeight: "bold",
    fontSize: "1.5em",
  },
  lastMessage: {
    fontStyle: "italic",
  },
});

export default useStyles;

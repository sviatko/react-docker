import { secondary } from "..";

import { makeStyles } from "@material-ui/core/styles";

const dividerStyles = {
  root: {
    textAlign: "center",
    marginBottom: "42px",
    position: "sticky",
    top: "12px",
    zIndex: 99,
    opacity: 0.6,
    clear: "both",
  },
  text: {
    padding: "1px 24px",
    backgroundColor: secondary[0],
    borderRadius: "50px",
    fontSize: "12px",
    color: secondary[1],
    fontStyle: "italic",
  },
};

export default makeStyles(dividerStyles);

import { makeStyles } from "@material-ui/core/styles";

import { secondary, defaultPadding } from "../index";

const useStyles = makeStyles({
  root: {
    cursor: "pointer",
    position: "relative",
    backgroundColor: secondary[0],
    color: secondary[2],
    padding: defaultPadding,
    width: "50%",
    minHeight: "24px",
    margin: "12px 0 42px",
  },
  avatar: {
    position: "absolute",
    width: "60px",
    height: "60px",
    top: "-30px",
    border: "4px solid #fff",
  },
  message: {
    margin: "0 0 0 84px",
  },
  time: {
    position: "absolute",
    right: "12px",
    bottom: 0,
    fontSize: "12px",
    fontStyle: "italic",
    color: secondary[1],
  },
  right: {
    display: "flex",
    justifyContent: "flex-end",
    "& p": {
      margin: 0,
    },
  },
});

export default useStyles;

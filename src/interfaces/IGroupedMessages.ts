export interface IGroupedMessages<T> {
  [key: string]: Array<T>;
}

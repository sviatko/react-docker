import { IMessage } from "../../interfaces/IMessage";
import { IChatState } from "../../interfaces/IChatState";
import { ActionTypes } from "../actions/actionTypes";
import { me, sortMessages } from "../../services/MessageService";

type Action = {
  type: ActionTypes;
  payload: Array<IMessage> | IMessage | string;
};

const initialState: IChatState = {
  messages: [],
  currentMessage: "",
  editingMessage: null,
  isEditing: false,
};

const reducer = (state: IChatState, action: Action) => {
  switch (action.type) {
    case ActionTypes.SET_MESSAGES:
      return setMessages(state, action);
    case ActionTypes.EDIT_MESSAGE:
      return editMessage(state, action);
    case ActionTypes.STORE_MESSAGE:
      return storeMessage(state);
    case ActionTypes.DELETE_MESSAGE:
      return removeMessage(state, action);
    case ActionTypes.TOGGLE_EDITNG:
      return toggleEditing(state);
    case ActionTypes.SUBMIT_EDITNG:
      return submitEditing(state);
    case ActionTypes.SET_CURRENT_MESSAGE:
      return setCurrentMessage(state, action);
    default:
      return initialState;
  }
};

const setMessages = (state: IChatState, action: Action): IChatState => {
  if (!Array.isArray(action.payload)) {
    throw new Error("Expected to get an array of IMessages");
  }

  const messages = sortMessages([...action.payload]);

  return { ...state, messages: messages };
};

const storeMessage = (state: IChatState): IChatState => {
  const messages = [...state.messages];
  const currentMessage = state.currentMessage;
  const iam = me(messages);
  const newMessage = iam ? { ...iam } : null;

  if (newMessage && currentMessage !== "") {
    newMessage.text = currentMessage;
    newMessage.createdAt = new Date().toISOString();

    messages.push(newMessage);
  }

  return {
    ...state,
    messages: [...messages],
    currentMessage: "",
  };
};

const editMessage = (state: IChatState, action: Action): IChatState => {
  if (Array.isArray(action.payload) || typeof action.payload === "string") {
    throw new Error("Expected to get an object of IMessages");
  }
  const selectedMsg = { ...action.payload };

  return {
    ...state,
    isEditing: true,
    editingMessage: selectedMsg,
    currentMessage: selectedMsg.text,
  };
};

const removeMessage = (state: IChatState, action: Action): IChatState => {
  if (Array.isArray(action.payload) || typeof action.payload === "string") {
    throw new Error("Expected to get an object of IMessages");
  }

  const messageToDelete = action.payload;

  return {
    ...state,
    messages: [
      ...state.messages.filter((msg) => msg.id !== messageToDelete.id),
    ],
  };
};

const setCurrentMessage = (state: IChatState, action: Action): IChatState => {
  if (typeof action.payload !== "string") {
    throw new Error("Expected to get an IMessage object");
  }
  return {
    ...state,
    currentMessage: action.payload,
  };
};

const toggleEditing = (state: IChatState): IChatState => {
  return {
    ...state,
    isEditing: false,
    editingMessage: null,
    currentMessage: "",
  };
};

const submitEditing = (state: IChatState): IChatState => {
  const messages = [...state.messages];
  const editingMessage = state.editingMessage;
  const currentMessage = state.currentMessage;

  messages.forEach((msg) => {
    if (editingMessage) {
      if (msg.id === editingMessage.id) {
        msg.text = currentMessage;
        msg.editedAt = new Date().toDateString();
      }
    }

    return msg;
  });

  return {
    ...state,
    messages,
    isEditing: false,
    editingMessage: null,
    currentMessage: "",
  };
};

export default reducer;

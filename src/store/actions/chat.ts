import { ActionTypes } from "./actionTypes";
import { IMessage } from "../../interfaces/IMessage";

export const setMessages = (messages: Array<IMessage>) => {
  return {
    type: ActionTypes.SET_MESSAGES,
    payload: [...messages],
  };
};

export const storeMessage = () => {
  return {
    type: ActionTypes.STORE_MESSAGE,
  };
};

export const editMessage = (message: IMessage) => {
  return {
    type: ActionTypes.EDIT_MESSAGE,
    payload: { ...message },
  };
};

export const deleteMessage = (message: IMessage) => {
  return {
    type: ActionTypes.DELETE_MESSAGE,
    payload: { ...message },
  };
};

export const toggleEditing = () => {
  return {
    type: ActionTypes.TOGGLE_EDITNG,
  };
};

export const submitEditing = () => {
  return {
    type: ActionTypes.SUBMIT_EDITNG,
  };
};

export const setCurrentMessage = (message: string) => {
  return {
    type: ActionTypes.SET_CURRENT_MESSAGE,
    payload: message,
  };
};

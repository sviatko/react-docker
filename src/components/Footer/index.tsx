import React from "react";

import useStyles from "../../assets/jss/components/footerStyle";

const Footer = () => {
  const classes = useStyles();
  return <footer className={classes.root}>&copy; 2020 Copyright</footer>;
};

export default Footer;

import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";

import { Container } from "@material-ui/core";
import moment from "moment";

import * as actionCreator from "../../store/actions/index";

import Header from "../Header";
import Content from "../Content";
import Modal from "../Modal";
import Controls from "../Controls";

import { IMessage } from "../../interfaces/IMessage";
import { IChatState } from "../../interfaces/IChatState";
import { getMessages } from "../../services/ApiService";
import {
  getTotaUsers,
  getLastSentMessage,
} from "../../services/MessageService";
import { upArrowKeyboardClick } from "../../services/ListenerService";
import data from "../../data.json";

type ChatProps = {
  isEditing: boolean;
  messages: Array<IMessage>;
  currentMessage: string;
  setMessages: (messages: Array<IMessage>) => void;
  editMessage: (message: IMessage) => void;
  toggleEditing: () => void;
  submitEditing: () => void;
  setCurrentMessage: (message: string) => void;
};

const Chat = ({
  messages,
  currentMessage,
  isEditing,
  setMessages,
  editMessage,
  toggleEditing,
  submitEditing,
  setCurrentMessage,
}: ChatProps) => {
  useEffect(() => {
    setMessages(data);
    const lastSentMessage = getLastSentMessage(data);

    if (lastSentMessage) {
      upArrowKeyboardClick(() => editMessage(lastSentMessage));
    }
  }, [setMessages, editMessage]);

  const messagesTotal = messages.length;
  const usersTotal = getTotaUsers(messages);
  const lastMessageAt =
    messagesTotal > 0
      ? moment(messages[messagesTotal - 1].createdAt).format("H:m")
      : "00:00";

  return (
    <Container maxWidth="md">
      <Header
        title="Chat name"
        messagesTotal={messagesTotal}
        participantsTotal={usersTotal}
        lastMessageAt={lastMessageAt}
      />
      <Content />
      <Controls />
      <Modal
        text={currentMessage || ""}
        open={isEditing}
        close={toggleEditing}
        submit={submitEditing}
        onChange={(e) => setCurrentMessage(e.target.value)}
      />
    </Container>
  );
};

const mapStateToProps = (state: { chat: IChatState }) => {
  return {
    messages: state.chat.messages,
    isEditing: state.chat.isEditing,
    currentMessage: state.chat.currentMessage,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    setMessages: (messages: Array<IMessage>) =>
      dispatch(actionCreator.setMessages(messages)),
    editMessage: (message: IMessage) =>
      dispatch(actionCreator.editMessage(message)),
    toggleEditing: () => dispatch(actionCreator.toggleEditing()),
    submitEditing: () => dispatch(actionCreator.submitEditing()),
    setCurrentMessage: (text: string) =>
      dispatch(actionCreator.setCurrentMessage(text)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);

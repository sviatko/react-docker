import React, { ChangeEvent } from "react";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
  FormControl,
  InputLabel,
  Input,
} from "@material-ui/core";

import useStyles from "../../assets/jss/components/controlsStyle";

type Props = {
  text: string;
  open: boolean;
  close: () => void;
  submit: () => void;
  onChange: (
    event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => void;
};

const Modal = ({ text, open, close, submit, onChange }: Props) => {
  const classes = useStyles();
  return (
    <Dialog
      maxWidth={"md"}
      fullWidth
      open={open}
      onClose={close}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">Edit message</DialogTitle>
      <DialogContent>
        <FormControl fullWidth>
          <InputLabel
            variant={"outlined"}
            htmlFor="message"
            classes={{ focused: classes.focused }}
          >
            Enter Your Message
          </InputLabel>
          <Input
            id="message"
            fullWidth
            autoComplete={"off"}
            value={text}
            onChange={onChange}
            classes={{
              underline: classes.underline,
            }}
          />
        </FormControl>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={close}
          color="primary"
          classes={{
            textPrimary: classes.focused,
          }}
        >
          Cancel
        </Button>
        <Button
          onClick={submit}
          color="primary"
          variant={"outlined"}
          classes={{ outlinedPrimary: classes.outlinedPrimary }}
        >
          Update
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default Modal;

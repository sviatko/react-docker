import React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";

import {
  Grid,
  Input,
  InputLabel,
  FormControl,
  Card,
  CardContent,
  Button,
} from "@material-ui/core";

import useStyles from "../../assets/jss/components/controlsStyle";
import { IChatState } from "../../interfaces/IChatState";
import * as actionCreator from "../../store/actions";

type Props = {
  currentMessage: string;
  isEditing: boolean;
  setCurrentMessage: (message: string) => void;
  setMessage: () => void;
};

const Controls = ({
  currentMessage,
  isEditing,
  setCurrentMessage,
  setMessage,
}: Props) => {
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <CardContent>
        <Grid
          container
          justify={"space-between"}
          alignItems={"flex-end"}
          spacing={3}
        >
          <Grid item md={10} sm={10}>
            <FormControl fullWidth>
              <InputLabel
                variant={"outlined"}
                htmlFor="message"
                classes={{ focused: classes.focused }}
              >
                Enter Your Message
              </InputLabel>
              <Input
                id="message"
                fullWidth
                autoComplete={"off"}
                value={!isEditing ? currentMessage : ""}
                onChange={(e) => setCurrentMessage(e.target.value)}
                classes={{
                  underline: classes.underline,
                }}
              />
            </FormControl>
          </Grid>
          <Grid item md={2} sm={2}>
            <Button
              variant="outlined"
              color="primary"
              onClick={setMessage}
              classes={{ outlinedPrimary: classes.outlinedPrimary }}
            >
              Send
            </Button>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

const mapStateToProps = (state: { chat: IChatState }) => {
  return {
    currentMessage: state.chat.currentMessage,
    isEditing: state.chat.isEditing,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    setCurrentMessage: (message: string) =>
      dispatch(actionCreator.setCurrentMessage(message)),
    setMessage: () => dispatch(actionCreator.storeMessage()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Controls);
